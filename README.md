# express实践demo: 记账本

## 技术栈
`express-generator` + `lowdb` + `ejs` + `shortid`

## 功能
- 支持添加/删除记账记录
- 记账记录支持选择事项, 记账时间, 选择支出/收入类型, 备注
- 支出和收入类型不同样式
