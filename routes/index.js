const express = require('express');
const path = require('path')
const shortId = require('shortid')
var router = express.Router();

const low = require('lowdb')

const FileSync = require('lowdb/adapters/FileSync');
const { debugPort } = require('process');
const adapters = new FileSync(path.resolve(__dirname, '../db/db.json'))
const db = low(adapters)

// 记账本列表
router.get('/account', function(req, res, next) {
  const arr = db.get('accounts').value()
  console.log('arr', arr)
  res.render('list', {list: arr})
});

// 添加记录
router.get('/account/create', function(req, res, next) {
  res.render('create')
})

// 新增记录
router.post('/account', (req, res) => {
  console.log(req.body)
  // 写入文件
  let id = shortId.generate()
  db.get('accounts').unshift({id, ...req.body}).write()
  res.render('success', {msg: '添加成功哦', url: '/account'})
})

// 删除记录
router.get('/account/:id', (req, res) => {
  console.log('req.params', req.params)
  const id = req.params.id
  db.get('accounts').remove({id}).write()
  res.render('success', {msg: '删除成功', url: '/account'})
})

module.exports = router;
